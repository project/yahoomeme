<?php


function yahoomeme_meme( $username = NULL, $category = NULL) {
  $output = '';
  $where = '';
  $args = array();
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('meme'),meme);
  if (isset($username)) {
    drupal_set_title(drupal_ucfirst($username));
    $where = " WHERE p.meme_username='%s'";
    $args[] = $username;
    $breadcrumb[] = $username;
    $sql = "SELECT * FROM {memeuser} WHERE meme_username='%s'";

    $a = db_fetch_object(db_query($sql, $username));
    $result[] = array(
      theme('image', $a->avatar_url, '', '', array(), FALSE),
      $username,
      l(drupal_ucfirst($a->title), 'http://meme.yahoo.com/'.$username),
      $a->description,
      $a->followers.' '.l('Followers', 'http://meme.yahoo.com/'.$username.'/followers'),
      );
    $output = theme('table', array(),$result);
  }

  drupal_set_breadcrumb($breadcrumb);
  
  $rows_per_page = variable_get('yahoomeme_rows_per_page', 10);
  $header = array(
    array('data' => t('Username'), 'field' => 'p.meme_username', 'colspan' => 2),
    array('data' => t('Posts'), 'field' => 'p.content'),
    array('data' => t('Date'), 'field' => 'p.created_time', 'sort' => 'desc'),      
  );
  
  $query = "SELECT p.*, u.avatar_url FROM {memepost} p";
  $query .= " LEFT JOIN {memeuser} u USING(meme_username) ";
  $query .= $where;
  $query .= tablesort_sql($header);
  $sql = pager_query($query, $rows_per_page, 0, NULL, $args);
  while($post = db_fetch_object($sql)) {
    if ($post->category == 'photo') {
      $meme = theme('image', $post->content, '', '', array(), FALSE);
    }
    else {
      $meme = $post->content;
    }
    $rows[] = array(
      array('data' => theme('image', $post->avatar_url, '', '', array(), FALSE)),
      array('data' => l($post->meme_username,'meme/'.$post->meme_username), 'align' => 'center'),
      array('data' => $meme),
      array('data' => format_interval(time() - $post->created_time ). ' ago'),
    );
  }
  
  if ($rows) {
    $output .= theme('table', $header, $rows);
    $output .= theme('pager', $rows_per_page);
  }
  else {
    $output = 'No Memepost to display';
  }
  return $output;
}

function yahoomeme_usermeme( $username, $category = NULL) {
  $breadcrumb[] = l(t('Home'), NULL);
  $breadcrumb[] = l(t('meme'),meme);
  $breadcrumb[] = $username;
  drupal_set_breadcrumb($breadcrumb);
  

  $sql = "SELECT * FROM {memeuser} WHERE meme_username='%s'";
  $a = db_fetch_object(db_query($sql, $username));
  
  $result[] = array(
    theme('image', $a->avatar_url, '', '', array(), FALSE),
    l(drupal_ucfirst($a->title), 'http://meme.yahoo.com/'.$username),
    $a->description,
    $a->followers.' '.l('Followers', 'http://meme.yahoo.com/'.$username.'/followers'),
  );
  
  $output = theme('table', array(),$result);
  
  $rows_per_page = variable_get('yahoomeme_rows_per_page', 10);
  $header = array(
    array('data' => t('Date'), 'field' => 'created_time', 'sort' => 'desc'),      
    array('data' => t('Posts'), 'field' => 'content'),
  );
  
  $query = "SELECT * FROM {memepost}";
  $query .= " WHERE meme_username='%s'";
  $query .= tablesort_sql($header);
  $args = array($username);
  $sql = pager_query($query, $rows_per_page, 0, NULL, $args);
  
  while($post = db_fetch_object($sql)) {
    if ($post->category == 'photo') {
      $meme = theme('image', $post->content, '', '', array(), FALSE);
    }
    else {
      $meme = $post->content;
    }
    $rows[] = array(
      array('data' => format_date($post->created_time, 'custom', 'd M')),
      array('data' => $meme),
    );
  }
  $output .= theme('table', $header, $rows, array());
  $output .= theme('pager', $rows_per_page);
  return $output;
}

function yahoomeme_user_meme($user_account) {
  if (empty($user_account)) {
    global $user;
    $user_account = $user;
  }
  drupal_set_title($user_account->name.' Memes');
    
  $rows_per_page = variable_get('yahoomeme_rows_per_page', 10);
  $header = array(
    array('data' => t('Username'), 'field' => 'p.meme_username', 'colspan' => 2),
    array('data' => t('Posts'), 'field' => 'p.content'),
    array('data' => t('Date'), 'field' => 'p.created_time', 'sort' => 'desc'),      
  );
  $query = "SELECT * FROM {memeuser} u ";
  $query .= " LEFT JOIN {memepost} p USING(meme_username) ";
  $query .= " WHERE u.uid=%d";
  $query .= tablesort_sql($header);
  $args[] = $user_account->uid;
  $sql = pager_query($query, $rows_per_page, 0, NULL, $args);
   
  while($post = db_fetch_object($sql)) {
    if ($post->category == 'photo') {
      $meme = theme('image', $post->content, '', '', array(), FALSE);
    }
    else {
      $meme = $post->content;
    }
    $rows[] = array(
      array('data' => theme('image', $post->avatar_url, '', '', array(), FALSE)),
      array('data' => l($post->meme_username,'meme/'.$post->meme_username), 'align' => 'center'),
      array('data' => $meme),
      array('data' => format_interval(time() - $post->created_time ). ' ago'),
    );
  }
  if ($rows) {
    $output .= theme('table', $header, $rows, array());
    $output .= theme('pager', $rows_per_page);
  }
  else {
    $output = t('No memepost to display');
  }
  return $output;
}

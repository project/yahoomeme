<?php


function _yahoomeme_admin_form() {
  $form = array();

  //TODO: Add a Global meme account for a site
/*
  $form['yahoomeme_site_account'] = array(
      '#type' => 'textfield',
      '#title' => t('Global Meme Account'),
      '#default_value' => variable_get('yahoomeme_site_account', ''),
      );
*/
  $periods = array(0 => t('Never'));
  $periods += drupal_map_assoc(array(604800, 2419200, 7257600, 31449600), 'format_interval');
  $form['yahoomeme_post_expire'] = array(
      '#type' => 'select',
      '#title' => t('Delete old statuses'),
      '#default_value' => variable_get('yahoomeme_post_expire', 0), 
      '#options' => $periods
      );
  $interval = array(0, 30*60, 60*60, 2*60*60, 3*60*60, 4*60*60, 6*60*60, 12*60*60, 24*60*60, 2*24*60*60, 3*24*60*60, 7*24*60*60, 14*24*60*60);
  $form['yahoomeme_cron_interval'] = array(
      '#type' => 'select',
      '#options' => drupal_map_assoc($interval, 'format_interval'),
      '#title' => t('Cron interval'),
      '#description' => t("This sets the frequency of cron run. An interval of 0 means that the posts are fetched every cron run."),
      '#size' => count($interval) / 2,
      '#default_value' => variable_get('yahoomeme_cron_interval', 0),
      );  
  $form['yahoomeme_users_per_cron'] = array(
      '#type' => 'textfield',
      '#title' => t('User Account Per Cron'),
      '#default_value' => variable_get('yahoomeme_users_per_cron', 10),
      '#size' => 8,
      '#description' => t('This sets the number of meme accounts that will be scanned for latest posts per cron.'),
      );
  $form['yahoomeme_rows_per_page'] = array(
      '#type' => 'textfield',
      '#title' => t('Post Per Page'),
      '#default_value' => variable_get('yahoomeme_rows_per_page', 10),
      '#size' => 8,
      '#description' => t('This sets the number of posts to display per page.'),
      );

  return system_settings_form($form);
}

function yahoomeme_user_settings($user_account) {
  module_load_include('inc', 'yahoomeme');
  $output = '';
  $output .= drupal_get_form('yahoomeme_add_account', $user_account);
  $meme_account = yahoomeme_meme_account($user_account);
  if( !empty($meme_account)) {
    $output .= drupal_get_form('yahoomeme_account_list_form', $meme_account);
  }
  return $output;
}

function yahoomeme_add_account($form_state, $user_account) {
  if (empty($user_account)) {
    global $user;
    $user_account = $user;
  }

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $user_account->uid,
    );
  $form['meme_username'] = array(
      '#title' => 'Yahoo Meme username',
      '#type' => 'textfield',
      '#required' => TRUE,
    );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add Account'),
    );
  return $form;
}

function yahoomeme_add_account_validate($form, &$form_state) {
  module_load_include('inc', 'yahoomeme');
  $meme_username = $form_state['values']['meme_username'];
  //If an account with that name is already registered OR If that account is a valid yahoo meme account
  if( !yahoomeme_account_check($meme_username))
    form_set_error("meme_username", t('Please check the name of your Yahoo Meme account'));

}

function yahoomeme_add_account_submit( $form, &$form_state) {
  module_load_include('inc', 'yahoomeme');
  yahoomeme_saveuser_account($form_state['values']);
}

function yahoomeme_account_list_form($form_state, $accounts) {

  $form['accounts'] = array();
  $form['#tree'] = TRUE;
  $form['#cache'] = TRUE;

  foreach ($accounts as $account) {
    $form['accounts'][] = _yahoomeme_account_row($account);
  }

  if (!empty($accounts)) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }

  return $form;
 
}

function _yahoomeme_account_row($account) {
  $form = array();
  $form['#account'] = $account;

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $account['uid'],
  );

  $form['meme_username'] = array(
      '#type' => 'value',
      '#value' => $account['meme_username'],
      );  

  $form['avatar'] = array(
    '#type' => 'markup',
    '#value' => theme('image', $account['avatar_url'], '', '', array(), FALSE),
  );

  $form['username_link'] = array(
    '#type' => 'markup',
    '#value' => l($account['meme_username'], 'meme/'.$account['meme_username']),
    );
    
  $form['title'] = array(
    '#type' => 'markup',
    '#value' => $account['title'],
    );
  
  $form['description'] = array(
    '#type' => 'markup',
    '#value' => filter_xss($account['description']),
    );

  $form['remove'] = array(
      '#type' => 'checkbox',
      '#default_value' => FALSE,
    );
  
  return $form;

}

function theme_yahoomeme_account_list_form($form) {
  $header = array(t('Avatar'), t('Username'), t('Name'), t('Description'), t('Remove'));
  $rows = array();
  foreach(element_children($form['accounts']) as $key) {
    $element = &$form['accounts'][$key];
    $rows[] = array(
        drupal_render($element['avatar']),
        drupal_render($element['uid']) . drupal_render($element['username_link']) . drupal_render($element['meme_username']) ,
        drupal_render($element['title']),
        drupal_render($element['description']),
        drupal_render($element['remove']),
    );
  }
  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}

function yahoomeme_account_list_form_submit($form, &$form_state) {
  $accounts = $form_state['values']['accounts'];
  foreach($accounts as $account) {
    if( !empty($account['remove']) ) {
      yahoomeme_deleteuser_account($account['uid'], $account['meme_username']);
    }
  }
}

<?php



function _yahoomeme_convert_xml($request_url) {
  $answer = drupal_http_request($request_url);
  if ($answer->code == '200') {
    $xmlparse = simplexml_load_string($answer->data);
    return $xmlparse;
  }
  else
    return '';
}

function yahoomeme_account_check($meme_username) {
  $url = "http://query.yahooapis.com/v1/public/yql?q=select%20guid%20from%20meme.info%20where%20name%3D%27$meme_username%27%3B&format=xml";
  $result = _yahoomeme_convert_xml($url);
  $valid = isset($result->results->meme->guid)? 1: 0;
  return $valid;
}

function yahoomeme_saveuser_account($account_info) {
  $info = yahoomeme_get_user_info($account_info['meme_username']);
  $account = array();
  $account['uid'] = $account_info['uid'];
  $account['meme_username'] = $account_info['meme_username'];
  $account['last_checked'] = time();
  $account += $info;
  drupal_write_record('memeuser', $account);
  return TRUE;
}

function yahoomeme_get_user_info($username) {
  $url = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20meme.info%20where%20name%3D%27$username%27&format=xml";
  $result = _yahoomeme_convert_xml($url);
  $info = array();
  $info['guid'] = $result->results->meme->guid;
  $info['title'] = $result->results->meme->title;
  $info['description'] = $result->results->meme->description;
  $info['avatar_url'] = $result->results->meme->avatar_url;
  $info['followers'] = $result->results->meme->followers;
  return $info;
}

function yahoomeme_update_user_account($account) {
  $username = $account['meme_username'];
  $url = "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20meme.info%20where%20name%3D%27$username%27&format=xml";
  $result = _yahoomeme_convert_xml($url);
  $account['guid'] = $result->results->meme->guid;
  $account['title'] = $result->results->meme->title;
  $account['description'] = $result->results->meme->description;
  $account['avatar_url'] = $result->results->meme->avatar_url;
  $account['followers'] = $result->results->meme->followers;
  drupal_write_record('memeuser', $account, 'meme_username');
  return $info;
}

function yahoomeme_meme_account($user_account) {
  $result = db_query("SELECT uid,avatar_url,meme_username,title,description FROM {memeuser} WHERE uid='%d'", $user_account->uid);
  while ($account = db_fetch_array($result)) {
    $accounts[$account['meme_username']] = $account;
  }
  return $accounts;
}


function yahoomeme_deleteuser_account($uid, $meme_username = '') {
  
  $query = "DELETE FROM {memeuser} WHERE uid=%d ";
  $args[] = $uid;
  if (!empty($meme_username))  {
    $query .= " AND meme_username='%s'";
    $args[] = $meme_username;
    $query1 = "DELETE FROM {memepost} WHERE meme_username='%s'";
    db_query($query1, $meme_username);
  }
  db_query($query, $args);
}

function yahoomeme_fetch_guid_from_username($username) {
  return  db_result(db_query("SELECT guid FROM {memeuser} WHERE meme_username='%s'", $username));
}

function yahoomeme_get_last_fetched_post_timestamp($username) {
  return db_result(db_query("SELECT max(timestamp) FROM {memepost} WHERE meme_username='%s'", $username));
}

function yahoomeme_fetch_user_meme($meme_username) {
  $guid = yahoomeme_fetch_guid_from_username($meme_username);
  $timestamp = yahoomeme_get_last_fetched_post_timestamp($meme_username);
  $timestamp = $timestamp? $timestamp: 0;
  $request_url = "http://query.yahooapis.com/v1/public/yql?q=SELECT%20*%20FROM%20meme.posts%20WHERE%20owner_guid%3D'$guid'%20and%20timestamp%20%3E%20$timestamp%3B&format=xml";

  $data = _yahoomeme_convert_xml($request_url);
  if(isset($data->results)) {
    foreach($data->results->post as $post) {
      $val['pubid'] = $post->pubid;
      $val['meme_username'] = $meme_username;
      $val['url'] = $post->url;
      $val['category'] = $post->category;
      $val['content'] = $post->content;
      $val['created_time'] = (int)((double) $post->timestamp/1000);
      $val['timestamp'] = $post->timestamp;
      $val['caption'] = $post->caption;
      $val['comment'] = $post->comment;
      $val['repost_count'] = $post->repost_count;
      drupal_write_record('memepost', $val);
    }
  }

}
